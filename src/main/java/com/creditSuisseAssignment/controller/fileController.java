package com.creditSuisseAssignment.controller;

import com.creditSuisseAssignment.Entity.Events;
import com.creditSuisseAssignment.Utility.Parser;
import com.creditSuisseAssignment.repository.EventService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class fileController {
    private final Logger LOGGER = LoggerFactory.getLogger(fileController.class);
    @Autowired
    EventService eventService;

    @Value("${limitCountPerThread}")
    int limitCountPerThread;

    @Autowired
    Parser parser;

    @GetMapping("saveFile")
    public ResponseEntity saveFile(@RequestParam(value = "fileName") String fileName) {
        try {
            LOGGER.info("procces started");
            int count = 0;
            int maxCount = (int) Files.lines(Paths.get(fileName)).count();
            while (count < maxCount) {
                List<String> list = Files.lines(Paths.get(fileName)).skip(count).limit(limitCountPerThread).collect(Collectors.toList());//forEach(l -> LOGGER.info("Failed to parse file {}", l));
                List<Events> events = parser.parseLogs(list);
                events.forEach(System.out::println);
                count = count + limitCountPerThread;
                eventService.save(events);
                LOGGER.info("procces ended");
            }
        } catch (Exception e) {
            LOGGER.info("Exception while saving or Parsing data {}",e);
        }
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}
