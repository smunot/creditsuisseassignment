package com.creditSuisseAssignment.Service;

import com.creditSuisseAssignment.Entity.Events;
import com.creditSuisseAssignment.Utility.Parser;
import com.creditSuisseAssignment.repository.EventRepository;
import com.creditSuisseAssignment.repository.EventService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
public class EventServiceImpl implements EventService {
    private final Logger LOGGER = LoggerFactory.getLogger(EventServiceImpl.class);

    @Autowired
    EventRepository eventRepository;

    @Async
    public CompletableFuture<List<Events>> save(List<Events> events)  {
        LOGGER.info("Saving list logs");
        events = (List<Events>) eventRepository.saveAll(events);
        return CompletableFuture.completedFuture(events);
    }
}
