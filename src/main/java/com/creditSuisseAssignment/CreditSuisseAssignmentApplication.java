package com.creditSuisseAssignment;

import com.creditSuisseAssignment.Utility.Parser;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Scanner;

@SpringBootApplication
public class CreditSuisseAssignmentApplication {
	private final static Log logger = LogFactory.getLog(CreditSuisseAssignmentApplication.class);
	public static void main(String[] args) {
		SpringApplication.run(CreditSuisseAssignmentApplication.class, args);

//		Scanner scanner = new Scanner(System.in);
//		String file = scanner.nextLine();
//
//        if (file == null) {
//			logger.error("Arguments should be in the format --args='<File>'.");
//			throw new IllegalArgumentException("Please check the arguments and run again.");
//        }
//		System.out.println("Enter file Name");
//		logger.error("Enter file name");
//		Parser parser = new Parser();
//		try {
//			parser.parseAndSaveLogs(file);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}

	}

}
