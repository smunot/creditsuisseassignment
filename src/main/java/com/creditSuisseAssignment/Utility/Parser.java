package com.creditSuisseAssignment.Utility;

import com.creditSuisseAssignment.Entity.Events;
import com.creditSuisseAssignment.Service.EventServiceImpl;
import com.creditSuisseAssignment.models.Logs;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Component
public class Parser {

    ConcurrentMap<String, Logs> entryLogs = new ConcurrentHashMap<>();

    public List<Events> parseLogs(List<String> logsList) throws Exception {
        List<Events> events = new ArrayList<>();
        Gson gson = new Gson();
        for (String log : logsList) {
            Logs logs = gson.fromJson(log, Logs.class);
            String eventId = logs.getId();
            if (!entryLogs.containsKey(eventId)) {
                entryLogs.put(eventId, logs);
                continue;
            }
            Logs previousLog = entryLogs.remove(eventId);
            long timeDiff = Math.abs(logs.getTimestamp() - previousLog.getTimestamp());
            Boolean alert = false;
            if (timeDiff > 4) {
                alert = true;
            }
            events.add(new Events(eventId, timeDiff, alert, previousLog.getType(), previousLog.getHost()));
        }
        return events;
    }
}
