package com.creditSuisseAssignment.repository;

import com.creditSuisseAssignment.Entity.Events;

import java.util.List;
import java.util.concurrent.CompletableFuture;

public interface EventService {
    public CompletableFuture<List<Events>> save(List<Events> events);
}
