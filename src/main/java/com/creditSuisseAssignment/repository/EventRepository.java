package com.creditSuisseAssignment.repository;

import com.creditSuisseAssignment.Entity.Events;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EventRepository extends CrudRepository<Events,String> {
}
