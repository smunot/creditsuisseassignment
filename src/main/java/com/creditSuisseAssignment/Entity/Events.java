package com.creditSuisseAssignment.Entity;

import lombok.Getter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "event")
//@AllArgsConstructor
@Getter
@ToString
public class Events implements Serializable {
    @Id
    @GeneratedValue
    int id;

    @Column
    private String eventId;
    @Column
    private long duration;
    @Column
    private boolean alert;
    @Column
    private String type;
    @Column
    private String host;

    public Events(String id, long duration, boolean alert, String type, String host) {
        this.eventId = id;
        this.duration = duration;
        this.alert = alert;
        this.type = type;
        this.host = host;
    }
}
