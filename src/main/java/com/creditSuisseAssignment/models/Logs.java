package com.creditSuisseAssignment.models;

public class Logs {
    private long timestamp;
    private String id;
    private String state;
    private String host;
    private String type;

    public long getTimestamp() {
        return timestamp;
    }

    public String getId() {
        return id;
    }

    public String getState() {
        return state;
    }

    public String getHost() {
        return host;
    }

    public String getType() {
        return type;
    }

    public static class Builder {
        private long timestamp;
        private String id;
        private String state;
        private String host;
        private String type;

        public Builder(String id, String state, long timestamp) {
            this.id = id;
            this.state = state;
            this.timestamp = timestamp;
        }

        public Builder withHost(String host) {
            this.host = host;
            return this;
        }

        public Builder withType(String type) {
            this.type = type;
            return this;
        }

        public Logs build() {
            Logs logs = new Logs();
            logs.id = this.id;
            logs.state = this.state;
            logs.timestamp = this.timestamp;
            logs.type = this.type;
            logs.host = this.host;
            return logs;
        }
    }
}
